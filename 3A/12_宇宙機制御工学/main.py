import numpy as np
import scipy as sp
import scipy.optimize
import quaternion
import os
from util import np_3d, plot, runge_kutta, _print
from util import f_cos, f_sin, f2_cos, f2_sin

np.random.seed(0)  # シード固定

# ハイパーパラメタ設定
dt = 0.01  # s, 時間刻み幅
T = 50.  # s, シミュレーション時間
kalman_interval = 1.  # s, フィルタ間隔
sigma_w = 0.01  # 外乱トルクの標準偏差
sigma_v = 0.01  # 観測ノイズの標準偏差
sigma_init = 1.  # 初期値の標準偏差

# 定数
I = np_3d(1.9, 1.6, 2.0)  # kgm^2

# 初期値設定
omegas = 17 * (2 * np.pi / 60)  # rad/s

omega0: np_3d = np_3d(0.1, omegas + 0.1, 0)  # rad/s
omega0_hat: np_3d = np_3d(np.random.normal(0, sigma_init, 3))  # rad/s
q0: quaternion.quaternion = np.quaternion(1, 0, 0, 0)
q0_hat: quaternion.quaternion = np.quaternion(*np.random.normal(0, sigma_init, 4)).normalized()

x = np.append(q0.components, omega0)  # 真値
x_ideal = np.append(q0.components, omega0)  # 真値(外乱なし)
x_hat = np.append(q0_hat.components, omega0_hat)  # 推定値
delta_x = x - x_hat  # 真値と推定値の差分値

P = np.eye(7) * 0.1  # 共分散行列
Q = np.eye(3) * sigma_w ** 2  # 外乱トルクの分散の対角行列
R = np.eye(3) * sigma_v ** 2  # 観測ノイズの分散の対角行列

t: float = 0.  # 時間
count: int = 0  # カウント
kalman_interval_count = int(kalman_interval / dt)  # カルマンフィルタのカウント間隔

# ログ用配列
log_omega = [omega0]
log_omega_ideal = [omega0]
log_omega_hat = [omega0_hat]
log_q = [q0]
log_q_ideal = [q0]
log_q_hat = [q0_hat]
log_P = [np.sqrt(np.diag(abs(P)))]  # 共分散行列の対角成分のルート
log_t = [t]


def diff(x: np.ndarray, M: np.ndarray = np.zeros(3)):
    """状態の微分
    Args:
        x (np.ndarray): 状態
        M (np.ndarray): 外乱トルク
    Returns:
        dx (np.ndarray): 状態の微分
    """
    q = np.quaternion(*x[:4])
    omega = np_3d(*x[4:])
    M = np_3d(*M)

    # omegaの微分
    d_omega = np_3d()
    d_omega.x = (M.x - (I.z - I.y) * omega.y * omega.z) / I.x
    d_omega.y = (M.y - (I.x - I.z) * omega.z * omega.x) / I.y
    d_omega.z = (M.z - (I.y - I.x) * omega.x * omega.y) / I.z

    # qの微分
    d_q = np.array([
        [-q.x, -q.y, -q.z],
        [+q.w, -q.z, +q.y],
        [+q.z, +q.w, -q.x],
        [-q.y, +q.x, +q.w]
    ]) @ omega / 2

    dx = np.append(d_q, d_omega)
    return dx


def ekf(x: np.ndarray, delta_x: np.ndarray, P: np.ndarray, index: int):
    """拡張カルマンフィルタ
    Args:
        x (np.ndarray): 状態
        delta_x (np.ndarray): 真値と観測値の差分値
        P (np.ndarray): 共分散行列
        index (int): DCMのヤコビ行列の列ベクトルのindex
    Returns:
        delta_x_next (np.ndarray): 次の真値と観測値の差分値
        P (np.ndarray): 次の共分散行列
        G (np.ndarray): カルマンゲイン
    """
    q = np.quaternion(*x[:4])
    omega = np_3d(*x[4:])

    # 状態方程式の係数
    # dx/dt = Ax + Bw
    A = np.array([
        [0, -omega.x / 2, -omega.y / 2, -omega.z / 2, -q.x / 2, -q.y / 2, -q.z / 2],
        [+omega.x / 2, 0, +omega.z / 2, -omega.y / 2, +q.w / 2, -q.z / 2, +q.y / 2],
        [+omega.y / 2, -omega.z / 2, 0, +omega.x / 2, +q.z / 2, +q.w / 2, -q.x / 2],
        [+omega.z / 2, +omega.y / 2, -omega.x / 2, 0, -q.y / 2, +q.x / 2, +q.w / 2],
        [0, 0, 0, 0, 0, (I.y - I.z) / I.x * omega.z, (I.y - I.z) / I.x * omega.y],
        [0, 0, 0, 0, (I.z - I.x) / I.y * omega.z, 0, (I.z - I.x) / I.y * omega.x],
        [0, 0, 0, 0, (I.x - I.y) / I.z * omega.y, (I.x - I.y) / I.z * omega.x, 0],
    ])
    B = np.array([
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
        [1 / I.x, 0, 0],
        [0, 1 / I.y, 0],
        [0, 0, 1 / I.z],
    ])

    # 離散化した状態方程式の係数
    # x(k+1) = Phi(k)x(k) + Gamma(k)w(k)
    Phi = sp.linalg.expm(A * dt)
    Gamma = np.linalg.inv(A) @ (Phi - np.eye(7)) @ B

    # DCMのヤコビ行列
    H = 2 * np.array([
        [[+q.w, +q.x, -q.y, -q.z, 0, 0, 0],
         [+q.z, +q.y, +q.x, +q.w, 0, 0, 0],
         [-q.y, +q.z, -q.w, +q.x, 0, 0, 0]],
        [[-q.z, +q.y, +q.x, -q.w, 0, 0, 0],
         [+q.w, -q.x, +q.y, -q.z, 0, 0, 0],
         [+q.x, +q.w, +q.z, +q.y, 0, 0, 0]],
        [[+q.y, +q.z, +q.w, +q.x, 0, 0, 0],
         [-q.x, -q.w, +q.z, +q.y, 0, 0, 0],
         [+q.w, -q.x, -q.y, +q.z, 0, 0, 0]],
    ])[index]

    # --- 予測ステップ ---
    # 差分値の更新
    delta_x_next = Phi @ delta_x
    # 共分散行列の更新
    P = Phi @ P @ Phi.T + Gamma @ Q @ Gamma.T

    # --- フィルタリングステップ ---
    # カルマンゲイン
    G = (P @ H.T) @ np.linalg.inv(H @ P @ H.T + R)
    # 事後共分散行列
    P_next = (np.eye(7) - G @ H) @ P

    return delta_x_next, P, G, P_next


def observe(x: np.ndarray, index: int, v: np.ndarray = np.zeros(3)):
    """観測方程式
    Args:
        x (np.ndarray): 状態
        v (np.ndarray): 観測ノイズ
        index (int): DCMの列ベクトルのindex
    Returns:
        y(np.ndarray): 観測量
    """
    q = np.quaternion(*x[:4])

    # DCM
    C = np.array([
        [q.w**2 + q.x**2 - q.y**2 - q.z**2, 2 * (q.x * q.y + q.w * q.z), 2 * (q.x * q.z - q.w * q.y)],
        [2 * (q.x * q.y - q.w * q.z), q.w**2 - q.x**2 + q.y**2 - q.z**2, 2 * (q.y * q.z + q.w * q.x)],
        [2 * (q.x * q.z + q.w * q.y), 2 * (q.y * q.z - q.w * q.x), q.w**2 - q.x**2 - q.y**2 + q.z**2],
    ])[index]
    y = C + v
    return y


_print("Start calculation")

print(f"t = {t:>5.2f} [s], P_std = {np.sqrt(abs(P)).trace():.8f}")

while t < T:
    t += dt  # 時間更新
    count += 1  # カウント更新
    index = np.random.choice([0, 1, 2])  # DCMの列ベクトルのインデックス
    w = np.random.normal(0, sigma_w, 3)  # 外乱トルク
    v = np.random.normal(0, sigma_v, 3)  # 観測ノイズ

    x_next = runge_kutta(diff, x, dt, M=w)  # 真値更新
    x_ideal_next = runge_kutta(diff, x_ideal, dt)  # 真値(外乱なし)更新
    x_hat_next = runge_kutta(diff, x_hat, dt)  # 推定値更新
    delta_x_next, P, G, P_next = ekf(x_hat, delta_x, P, index)  # 差分値, 共分散行列, カルマンゲイン, 事後共分散行列更新

    if count % kalman_interval_count == 0:
        y = observe(x, index, v=v)  # 真値
        y_hat = observe(x_hat, index)  # 推定値
        z = y - y_hat  # 観測量の差分値

        delta_x_next += G @ z  # 観測量の差分値で差分値更新
        x_hat_next += delta_x_next  # 差分値で推定値補正
        P = P_next  # 事後共分散行列に

        delta_x_next *= 0  # 差分値リセット

        print(f"t = {t:>5.2f} [s], P_std = {np.sqrt(abs(P)).trace():.8f}")

    # クォータニオン正規化
    x_next[:4] = np.quaternion(*x_next[:4]).normalized().components
    x_ideal_next[:4] = np.quaternion(*x_ideal_next[:4]).normalized().components
    x_hat_next[:4] = np.quaternion(*x_hat_next[:4]).normalized().components

    # 各値更新
    delta_x, x, x_ideal, x_hat = delta_x_next, x_next, x_ideal_next, x_hat_next

    # ログ追加
    q, omega = np.quaternion(*x[:4]), np_3d(*x[4:])
    q_ideal, omega_ideal = np.quaternion(*x_ideal[:4]), np_3d(*x_ideal[4:])
    q_hat, omega_hat = np.quaternion(*x_hat[:4]), np_3d(*x_hat[4:])
    log_omega.append(omega)
    log_omega_ideal.append(omega_ideal)
    log_omega_hat.append(omega_hat)
    log_q.append(q)
    log_q_ideal.append(q_ideal)
    log_q_hat.append(q_hat)
    log_P.append(np.sqrt(np.diag(abs(P))))
    log_t.append(t)

_print("End of calculation")

# ログから配列作成
t_list = np.array([t for t in log_t])
omega_x_list = np.array([omega.x for omega in log_omega])
omega_y_list = np.array([omega.y for omega in log_omega])
omega_z_list = np.array([omega.z for omega in log_omega])
omega_x_ideal_list = np.array([omega.x for omega in log_omega_ideal])
omega_y_ideal_list = np.array([omega.y for omega in log_omega_ideal])
omega_z_ideal_list = np.array([omega.z for omega in log_omega_ideal])
omega_x_hat_list = np.array([omega.x for omega in log_omega_hat])
omega_y_hat_list = np.array([omega.y for omega in log_omega_hat])
omega_z_hat_list = np.array([omega.z for omega in log_omega_hat])
omega_x_delta_list = omega_x_list - omega_x_hat_list
omega_y_delta_list = omega_y_list - omega_y_hat_list
omega_z_delta_list = omega_z_list - omega_z_hat_list
omega_x_P_list = np.array(log_P)[:, 0]
omega_y_P_list = np.array(log_P)[:, 1]
omega_z_P_list = np.array(log_P)[:, 2]
q_0_list = np.array([q.w for q in log_q])
q_1_list = np.array([q.x for q in log_q])
q_2_list = np.array([q.y for q in log_q])
q_3_list = np.array([q.z for q in log_q])
q_0_ideal_list = np.array([q.w for q in log_q_ideal])
q_1_ideal_list = np.array([q.x for q in log_q_ideal])
q_2_ideal_list = np.array([q.y for q in log_q_ideal])
q_3_ideal_list = np.array([q.z for q in log_q_ideal])
q_0_hat_list = np.array([q.w for q in log_q_hat])
q_1_hat_list = np.array([q.x for q in log_q_hat])
q_2_hat_list = np.array([q.y for q in log_q_hat])
q_3_hat_list = np.array([q.z for q in log_q_hat])
q_0_delta_list = q_0_list - q_0_hat_list
q_1_delta_list = q_1_list - q_1_hat_list
q_2_delta_list = q_2_list - q_2_hat_list
q_3_delta_list = q_3_list - q_3_hat_list
q_0_P_list = np.array(log_P)[:, 3]
q_1_P_list = np.array(log_P)[:, 4]
q_2_P_list = np.array(log_P)[:, 5]
q_3_P_list = np.array(log_P)[:, 6]

os.makedirs("imgs", exist_ok=True)

# 真値(外乱なし)の描画
plot(t_list, omega_x_ideal_list, r'$\omega_x$', "imgs/omega.x.png", ylabel=r'$\omega_x$')
plot(t_list, omega_y_ideal_list, r'$\omega_y$', "imgs/omega.y.png", ylabel=r'$\omega_y$')
plot(t_list, omega_z_ideal_list, r'$\omega_z$', "imgs/omega.z.png", ylabel=r'$\omega_z$')
plot(t_list, [omega_x_ideal_list, omega_y_ideal_list, omega_z_ideal_list], [
     r'$\omega_x$', r'$\omega_y$', r'$\omega_z$'], "imgs/omega.png", ylabel=r'$\omega$')
plot(t_list, q_0_ideal_list, r'$q_0$', "imgs/q.0.png", ylabel=r'$q_0$')
plot(t_list, q_1_ideal_list, r'$q_1$', "imgs/q.1.png", ylabel=r'$q_1$')
plot(t_list, q_2_ideal_list, r'$q_2$', "imgs/q.2.png", ylabel=r'$q_2$')
plot(t_list, q_3_ideal_list, r'$q_3$', "imgs/q.3.png", ylabel=r'$q_3$')
plot(t_list, [q_0_ideal_list, q_1_ideal_list, q_2_ideal_list, q_3_ideal_list],
     [r'$q_0$', r'$q_1$', r'$q_2$', r'$q_3'], "imgs/q.png", ylabel=r'$q$')

_print("End of plotting ideal curve")

# 近似曲線導出
omega_x_params, _ = sp.optimize.curve_fit(f_cos, t_list, omega_x_ideal_list, p0=[0.1, 0.36, 0])
omega_y_params, _ = sp.optimize.curve_fit(f_cos, t_list, omega_y_ideal_list, p0=[-0.0004, 0.7, -1.88])
omega_z_params, _ = sp.optimize.curve_fit(f_sin, t_list, omega_z_ideal_list, p0=[0.1, 0.36, 0])
q_0_params, _ = sp.optimize.curve_fit(f_cos, t_list, q_0_ideal_list, p0=[1., 1., 0])
q_1_params, _ = sp.optimize.curve_fit(f2_sin, t_list, q_1_ideal_list, p0=[0.03, 1, 0.03, 0.6])
q_2_params, _ = sp.optimize.curve_fit(f_sin, t_list, q_2_ideal_list, p0=[1., 1., 0])
q_3_params, _ = sp.optimize.curve_fit(f2_cos, t_list, q_3_ideal_list, p0=[-0.03, 1, 0.03, 0.6])

# 近似曲線描画
plot(t_list, [omega_x_ideal_list, f_cos(t_list, *omega_x_params)],
     [r"$\omega_x$", r"$\omega_{x_{fit}}$"], "imgs/omega.x.fit.png", ylabel=r'$\omega_x$')
plot(t_list, [omega_y_ideal_list, f_cos(t_list, *omega_y_params)],
     [r"$\omega_y$", r"$\omega_{y_{fit}}$"], "imgs/omega.y.fit.png", ylabel=r'$\omega_y$')
plot(t_list, [omega_z_ideal_list, f_sin(t_list, *omega_z_params)],
     [r"$\omega_z$", r"$\omega_{z_{fit}}$"], "imgs/omega.z.fit.png", ylabel=r'$\omega_z$')
plot(t_list, [q_0_ideal_list, f_cos(t_list, *q_0_params)],
     [r"$q_0$", r"$q_{0_{fit}}$"], "imgs/q.0.fit.png", ylabel=r'$q_0$')
plot(t_list, [q_1_ideal_list, f2_sin(t_list, *q_1_params)],
     [r"$q_1$", r"$q_{1_{fit}}$"], "imgs/q.1.fit.png", ylabel=r'$q_1$')
plot(t_list, [q_2_ideal_list, f_sin(t_list, *q_2_params)],
     [r"$q_2$", r"$q_{2_{fit}}$"], "imgs/q.2.fit.png", ylabel=r'$q_2$')
plot(t_list, [q_3_ideal_list, f2_cos(t_list, *q_3_params)],
     [r"$q_3$", r"$q_{3_{fit}}$"], "imgs/q.3.fit.png", ylabel=r'$q_3$')

_print("End of plotting fit curve")

# 近似曲線の式表示
print(f"omega_x = {omega_x_params[0]:.4f} x cos({omega_x_params[1]:.4f} t)")
print(f"omega_y = {omega_y_params[0]:.4f} x cos({omega_y_params[1]:.4f} t) + {omega_y_params[2]:.4f}")
print(f"omega_z = {omega_z_params[0]:.4f} x sin({omega_z_params[1]:.4f} t)")
print(f"q_0 = {q_0_params[0]:.4f} x cos({q_0_params[1]:.4f} t)")
print(f"q_1 = {q_1_params[0]:.4f} x sin({q_1_params[1]:.4f} t) + {q_1_params[2]:.4f} x sin({q_1_params[3]:.4f} t)")
print(f"q_2 = {q_2_params[0]:.4f} x sin({q_2_params[1]:.4f} t)")
print(f"q_3 = {q_3_params[0]:.4f} x cos({q_3_params[1]:.4f} t) + {q_3_params[2]:.4f} x cos({q_3_params[3]:.4f} t)")

_print("End of printing fit curve")

# 各周期の表示
print(f"period of omega_x: {2*np.pi/omega_x_params[1]:.5f} [s]")
print(f"period of omega_y: {2*np.pi/omega_y_params[1]:.5f} [s]")
print(f"period of q(1st): {2*np.pi/q_1_params[1]:.5f} [s]")
print(f"period of q(2nd): {2*np.pi/q_1_params[3]:.5f} [s]")

_print("End of printing period")

# 真値と推定値の描画
plot(t_list, [omega_x_list, omega_x_hat_list], [r'$\omega_x$', r'$\hat\omega_x$'],
     fname="imgs/omega.x.hat.png", ylabel=r'$\omega_x$')
plot(t_list, [omega_y_list, omega_y_hat_list], [r'$\omega_y$', r'$\hat\omega_y$'],
     fname="imgs/omega.y.hat.png", ylabel=r'$\omega_y$')
plot(t_list, [omega_z_list, omega_z_hat_list], [r'$\omega_z$', r'$\hat\omega_z$'],
     fname="imgs/omega.z.hat.png", ylabel=r'$\omega_z$')
plot(t_list, [q_0_list, q_0_hat_list], [r'$q_0$', r'$\hat q_0$'], fname="imgs/q.0.hat.png", ylabel=r'$q_0$')
plot(t_list, [q_1_list, q_1_hat_list], [r'$q_1$', r'$\hat q_1$'], fname="imgs/q.1.hat.png", ylabel=r'$q_1$')
plot(t_list, [q_2_list, q_2_hat_list], [r'$q_2$', r'$\hat q_2$'], fname="imgs/q.2.hat.png", ylabel=r'$q_2$')
plot(t_list, [q_3_list, q_3_hat_list], [r'$q_3$', r'$\hat q_3$'], fname="imgs/q.3.hat.png", ylabel=r'$q_3$')

_print("End of plotting estimated curve")

# 推定誤差とPの対角成分の描画
plot(t_list, [omega_x_delta_list, omega_x_P_list, -omega_x_P_list], [r'$\Delta\omega_x$',
     r'$\pm\sqrt{P_{\omega_x}}$', None], fname="imgs/omega.x.delta.png", color=["tab:blue", "tab:orange", "tab:orange"])
plot(t_list, [omega_y_delta_list, omega_y_P_list, -omega_y_P_list], [r'$\Delta\omega_y$',
     r'$\pm\sqrt{P_{\omega_y}}$', None], fname="imgs/omega.y.delta.png", color=["tab:blue", "tab:orange", "tab:orange"])
plot(t_list, [omega_z_delta_list, omega_z_P_list, -omega_z_P_list], [r'$\Delta\omega_z$',
     r'$\pm\sqrt{P_{\omega_z}}$', None], fname="imgs/omega.z.delta.png", color=["tab:blue", "tab:orange", "tab:orange"])
plot(t_list, [q_0_delta_list, q_0_P_list, -q_0_P_list], [r'$\Delta q_0$',
     r'$\pm\sqrt{P_{q_0}}$', None], fname="imgs/q.0.delta.png", color=["tab:blue", "tab:orange", "tab:orange"])
plot(t_list, [q_1_delta_list, q_1_P_list, -q_1_P_list], [r'$\Delta q_1$',
     r'$\pm\sqrt{P_{q_1}}$', None], fname="imgs/q.1.delta.png", color=["tab:blue", "tab:orange", "tab:orange"])
plot(t_list, [q_2_delta_list, q_2_P_list, -q_2_P_list], [r'$\Delta q_2$',
     r'$\pm\sqrt{P_{q_2}}$', None], fname="imgs/q.2.delta.png", color=["tab:blue", "tab:orange", "tab:orange"])
plot(t_list, [q_3_delta_list, q_3_P_list, -q_3_P_list], [r'$\Delta q_3$',
     r'$\pm\sqrt{P_{q_3}}$', None], fname="imgs/q.3.delta.png", color=["tab:blue", "tab:orange", "tab:orange"])

_print("End of plotting error curve")
