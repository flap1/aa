from sympy import symbols, Matrix, latex, init_printing
from sympy.physics.mechanics import dynamicsymbols

init_printing(use_latex=True)

t = symbols("t")
I_x, I_y, I_z = symbols("I_x, I_y, I_z")
M_x, M_y, M_z = dynamicsymbols("M_x, M_y, M_z")
q_0, q_1, q_2, q_3 = dynamicsymbols("q_0, q_1, q_2, q_3")
omega_x, omega_y, omega_z = dynamicsymbols(r"\omega_x, \omega_y, \omega_z")

x = Matrix([q_0, q_1, q_2, q_3, omega_x, omega_y, omega_z])
w = Matrix([M_x, M_y, M_z])
f = Matrix([
    [(-q_1 * omega_x - q_2 * omega_y - q_3 * omega_z) / 2],
    [(q_0 * omega_x - q_3 * omega_y + q_2 * omega_z) / 2],
    [(q_3 * omega_x + q_0 * omega_y - q_1 * omega_z) / 2],
    [(-q_2 * omega_x + q_1 * omega_y + q_0 * omega_z) / 2],
    [(M_x - (I_z - I_y) * omega_y * omega_z) / I_x],
    [(M_y - (I_x - I_z) * omega_z * omega_x) / I_y],
    [(M_z - (I_y - I_x) * omega_x * omega_y) / I_z]])

A = f.jacobian(x)
B = f.jacobian(w)

print("f\n", latex(f))
print("-" * 10)
print("A\n", latex(B))
print("-" * 10)
print("B\n", latex(B))
print("-" * 10)

h1 = Matrix([
    [q_0**2 + q_1**2 - q_2**2 - q_3**2],
    [2 * (q_1 * q_2 + q_0 * q_3)],
    [2 * (q_1 * q_3 - q_0 * q_2)],
])
h2 = Matrix([
    [2 * (q_1 * q_2 - q_0 * q_3)],
    [q_0**2 - q_1**2 + q_2**2 - q_3**2],
    [2 * (q_2 * q_3 + q_0 * q_1)],
])
h3 = Matrix([
    [2 * (q_1 * q_3 + q_0 * q_2)],
    [2 * (q_2 * q_3 - q_0 * q_1)],
    [q_0**2 - q_1**2 - q_2**2 + q_3**2],
])

c1 = h1.jacobian(x)
c2 = h2.jacobian(x)
c3 = h3.jacobian(x)

print("h1\n", latex(h1))
print("-" * 10)
print("h2:\n", latex(h2))
print("-" * 10)
print("h3:\n", latex(h3))
print("-" * 10)
print("c1:\n", latex(c1))
print("-" * 10)
print("c2:\n", latex(c2))
print("-" * 10)
print("c3:\n", latex(c3))
