# 宇宙機制御工学

汎用化を考えていなかったので設定ファイルを分離していない. 
初期値等はコード参照.

## 依存ライブラリのインストール

```bash
pip install -r requirements.txt
```

## 実行

```bash
python main.py
```

課題1, 課題2の両方が実行される.

## 詳細

ソースコードの詳細はコード内のコメントを参照されたい

## おまけ

`latex.py`にはsympyで一部の行列が実装されている. 以下を実行することでLaTeX形式の文字列が得られる.

```bash
python latex.py
```
