import numpy as np
import matplotlib.pyplot as plt


def runge_kutta(diff, x, dt, method="RK45", **kwargs):
    """ルンゲ=クッタ法
    Args:
        diff (function): 状態方程式の右辺
        x (np.ndarray): 現在の状態
        dt (float): 時間幅
        method (str): ルンゲ=クッタ法のアルゴリズム指定("normal", "RK23", "RK45")
        **kwargs: 関数に渡す引数
    Returns:
        x_next (np.ndarray): 次の状態
    """
    # 古典的ルンゲ=クッタ法
    if method == "normal":
        k1 = diff(x, **kwargs)
        k2 = diff(x + 0.5 * dt * k1, **kwargs)
        k3 = diff(x + 0.5 * dt * k2, **kwargs)
        k4 = diff(x + dt * k3, **kwargs)
        x_next = x + dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
        return x_next

    # Bogacki–Shampine法
    elif method == "RK23":
        A = np.array([
            [0, 0, 0],
            [1 / 2, 0, 0],
            [0, 3 / 4, 0]
        ])
        B = np.array([2 / 9, 1 / 3, 4 / 9])

    # ルンゲ=クッタ=フェールベルグ法
    elif method == "RK45":
        A = np.array([
            [0, 0, 0, 0, 0],
            [1 / 5, 0, 0, 0, 0],
            [3 / 40, 9 / 40, 0, 0, 0],
            [44 / 45, -56 / 15, 32 / 9, 0, 0],
            [19372 / 6561, -25360 / 2187, 64448 / 6561, -212 / 729, 0],
            [9017 / 3168, -355 / 33, 46732 / 5247, 49 / 176, -5103 / 18656]
        ])
        B = np.array([35 / 384, 0, 500 / 1113, 125 / 192, -2187 / 6784, 11 / 84])

    else:
        raise ValueError("Select a method from ['normal', 'RK23', 'RK45']")

    K = np.empty((len(A), len(x)))
    K[0] = diff(x, **kwargs)
    for i, a in enumerate(A[1:], start=1):
        dx = K[:i].T @ a[:i] * dt
        K[i] = diff(x + dx, **kwargs)
    x_next = x + dt * K.T @ B
    return x_next


class np_3d(np.ndarray):
    """np.ndarrayの拡張, .x.y.zによるアクセスを可能に"""
    def __new__(cls, x=0, y=0, z=0):
        if isinstance(x, np.ndarray):
            x, y, z = x
        obj = super().__new__(cls, (3,), np.float64, np.asarray((x, y, z)),
                              offset=0, strides=None, order=None)
        obj.x = x
        obj.y = y
        obj.z = z
        return obj

    @property
    def x(self):
        return self[0]

    @property
    def y(self):
        return self[1]

    @property
    def z(self):
        return self[2]

    @x.setter
    def x(self, x):
        self[0] = x

    @y.setter
    def y(self, y):
        self[1] = y

    @z.setter
    def z(self, z):
        self[2] = z


def plot(t, data, label, fname=None, title=None, ylabel=None, color=None):
    """plotの共通化
    Args:
        t (list[float]): 時間のリスト
        data (list[float] or list[list[float]]): プロットするデータ, 複数プロットする場合は2次元配列に
        label (str or list[str]): ラベル, 複数プロットする場合は配列に
        fname (str, optional): 保存するファイル名. DefaultはNoneで保存なし
        title (str, optional): タイトル. DefaultはNoneでタイトルなし
        ylabel (str, optional): y軸ラベル. DefaultはNoneでy軸ラベルなし
        color (str or list[str], optional): 色指定, 複数プロットする場合は配列に. DefaultはNoneで色指定なし
    """
    _, ax = plt.subplots()

    if np.array(data).ndim == 1:
        ax.plot(t, data, label=label, color=color)
    else:
        if color is None: color = [None] * len(data)
        for _data, _label, _color in zip(data, label, color):
            ax.plot(t, _data, label=_label, color=_color)
    ax.legend()
    ax.grid()
    ax.set_xlabel(r'$t$')
    if ylabel is not None: ax.set_ylabel(ylabel)
    if title is not None: ax.set_title(title)
    if fname is not None:
        plt.savefig(fname)
        print(f"'{fname}' saved")

    plt.tight_layout()
    plt.close()


def _print(msg: str):
    """===で囲ってprintする
    Args:
        msg (str): 表示したいメッセージ
    """
    print(f"{'-'*10} {msg} {'-'*10}")


def f_sin(t, a, b, c):
    """近似曲線導出用, sin"""
    return a * np.sin(b * t) + c


def f_cos(t, a, b, c):
    """近似曲線導出用, cos"""
    return a * np.cos(b * t) + c


def f2_sin(t, a, b, c, d):
    """近似曲線導出用, sin, sin"""
    return a * np.sin(b * t) + c * np.sin(d * t)


def f2_cos(t, a, b, c, d):
    """近似曲線導出用, cos, cos"""
    return a * np.cos(b * t) + c * np.cos(d * t)
